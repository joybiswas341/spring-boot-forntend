import { Component } from '@angular/core';
import { Person } from 'src/app/model/person.model';
import { InformationService } from 'src/app/service/information.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent {
  personModelList: Person[] = [];

  private apiUrl = 'https://person/get-list';

  constructor(private informationService: InformationService) {
    this.getList();
  }

  getList() {
    this.informationService.getList().subscribe((res) => {
      this.personModelList = res;

      console.log(res);
    });
  }
}
