import { Component } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { Person } from '../model/person.model';
import { InformationService } from '../service/information.service';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css'],
})
export class PersonComponent {
  personModel: Person = new Person();
  constructor(
    private informationService: InformationService,
    private router: Router
  ) {
    this.personModel.dob = new Date();
  }

  calculateAge(): void {
    let currentDate = moment();
    let date = moment(this.personModel.dob);
    let age = moment.duration(currentDate.diff(date)).years();
    this.personModel.age = age;
  }

  save() {
    this.informationService.save(this.personModel).subscribe((res) => {
      console.log(res);
      localStorage.setItem('personModel', JSON.stringify(this.personModel));
      this.router.navigate(['family-info/', res.id]);
    });
  }
}
