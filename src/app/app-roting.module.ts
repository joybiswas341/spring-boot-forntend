import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FamilyInfoComponent } from './family-info/family-info.component';
import { ListComponent } from './list/list/list.component';
import { PersonComponent } from './person/person.component';

const routes: Routes = [
  { path: '', component: PersonComponent, pathMatch: 'full' },
  {
    path: 'family-info/:id',
    component: FamilyInfoComponent
  },
  {
    path: 'info-list',
    component: ListComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
