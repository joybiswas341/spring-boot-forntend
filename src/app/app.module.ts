import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-roting.module';

import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { FamilyInfoComponent } from './family-info/family-info.component';
import { PersonComponent } from './person/person.component';
import { ListComponent } from './list/list/list.component';

@NgModule({
  declarations: [AppComponent, PersonComponent, FamilyInfoComponent, ListComponent],
  imports: [BrowserModule, AppRoutingModule, FormsModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
