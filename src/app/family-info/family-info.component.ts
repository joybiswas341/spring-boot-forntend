import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Person } from '../model/person.model';
import { InformationService } from '../service/information.service';

@Component({
  selector: 'app-family-info',
  templateUrl: './family-info.component.html',
  styleUrls: ['./family-info.component.css'],
})
export class FamilyInfoComponent {
  personModel: Person = new Person();
  id: any;

  constructor(
    private informationService: InformationService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.route.params.subscribe((params) => {
      this.id = params['id'];
      console.log(this.id);
    });
  }

  update() {
    this.personModel.id = this.id;
    var storedPersonModel = localStorage.getItem('personModel');
    var personModel = null;
    if (storedPersonModel !== null) {
      personModel = JSON.parse(storedPersonModel);
    }
    this.setTabAData(personModel);
    console.log(this.personModel);

    this.informationService.update(this.personModel).subscribe((res) => {
      console.log(res);
      this.router.navigate(['info-list/']);
    });
    
  }

  setTabAData(personModel: any) {
    this.personModel.name = personModel.name;
    this.personModel.nameNative = personModel.nameNative;
    this.personModel.dob = personModel.dob;
    this.personModel.birthPlace = personModel.birthPlace;
    this.personModel.identification = personModel.identification;
    this.personModel.religion = personModel.religion;
    this.personModel.nationality = personModel.nationality;
    this.personModel.meansNationality = personModel.meansNationality;
    this.personModel.height = personModel.height;
    this.personModel.weight = personModel.weight;
    this.personModel.chest = personModel.chest;
    this.personModel.rightEye = personModel.rightEye;
    this.personModel.leftEye = personModel.leftEye;
    this.personModel.colourBlindNess = personModel.colourBlindNess;
    this.personModel.extraCurricular = personModel.extraCurricular;
    this.personModel.firstPreference = personModel.firstPreference;
    this.personModel.secondPreference = personModel.secondPreference;
    this.personModel.address = personModel.address;
    this.personModel.personEdu = personModel.personEdu;
    this.personModel.age = personModel.age;
  }
}
