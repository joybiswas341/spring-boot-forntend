import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InformationService {

  BASE_URL = "http://localhost:9100/";
  constructor(
    private http: HttpClient,
  ) { }

  save(data: any): Observable<any> {
    return this.http.post<any>(this.BASE_URL + 'person/save', data);
  }
  update(data: any): Observable<any> {
    return this.http.put<any>(this.BASE_URL + 'person/update', data);
  }
  getList(): Observable<any> {
    return this.http.get<any>(this.BASE_URL + 'person/get-list');
  }



}
